import { OverlayContext } from "@/components/Overlay";
import { useContext } from "react";
import { InstallContext } from "./InstallFlow";

export default function Overview() {
  const overlayContext = useContext(OverlayContext);
  const installContext = useContext(InstallContext);

  if (!installContext.site) return <></>
  
  return (
    <>
      <div>Overview</div>
      <div>Consumer: {`${installContext.site.consumer.firstName} ${installContext.site.consumer.lastName}`}</div>
      <button onClick={() => overlayContext.push('NewInstall')}>New Install</button>
    </>
  );
}

Overview.title = 'Overview'