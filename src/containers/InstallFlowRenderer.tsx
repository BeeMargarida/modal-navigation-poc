import { useContext } from "react";

import { OverlayContext } from "@/components/Overlay";
import Overview from "./Overview";
import NewInstall from "./NewInstall";
import InstallForm from "@/components/installation/InstallForm";
import Pod from "@/components/installation/Pod";
import Battery from "@/components/installation/Battery";
import Inverter from "@/components/installation/Inverter";

export const InstallComponents: { [key: string]: () => JSX.Element } = {
    'Overview': Overview,
    'NewInstall': NewInstall,
    'InstallForm': InstallForm,
    'Pod': Pod,
    'Battery': Battery,
    'Inverter': Inverter,
}

export default function InstallFlowRenderer() {
    const overlayContext = useContext(OverlayContext);

    const Component = InstallComponents[overlayContext.currentRoute] ?? (() => <></>)

    return (
        <Component />
    );
}
