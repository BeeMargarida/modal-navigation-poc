import { Dispatch, SetStateAction, createContext, useState } from "react";

import Overlay from "@/components/Overlay";
import InstallFlowRenderer, { InstallComponents } from "./InstallFlowRenderer";

const HARDCODED_SITE = {
    id: 1,
    uuid: 'ASDA123asd',
    consumer: {
        firstName: 'Josh',
        lastName: 'Doe',
        uuid: 'Qpqwe1e2'
    }
}

type Props = {
    onClose: () => void
}

type InstallState = {
    type?: 'spoke' | 'hub'
    pod?: { uuid: string }
    battery?: { serialNumber: string, capacity?: number }
    panel?: { serialNumber: string, capacity?: number }
    inverter?: { serialNumber: string, capacity?: number }
}

type InstallContext = {
    site?: typeof HARDCODED_SITE
    state: InstallState
    setState: Dispatch<SetStateAction<InstallState>>
    completeInstall: () => void
}

export const InstallContext = createContext<InstallContext>({
    state: {},
    setState: () => { },
    completeInstall: () => {}
})

export default function InstallFlow({ onClose }: Props) {
    const [state, setState] = useState<InstallState>({})

    const completeInstall = () => {}
    
    return (
        <Overlay<keyof typeof InstallComponents> firstRoute={'Overview'} onClose={onClose}>
            <InstallContext.Provider value={{
                site: HARDCODED_SITE,
                state: state,
                setState: setState,
                completeInstall: completeInstall
            }}>
                <InstallFlowRenderer />
            </InstallContext.Provider>
        </Overlay>
    );
}
