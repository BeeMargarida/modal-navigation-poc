import { OverlayContext } from "@/components/Overlay";
import { useContext } from "react";
import { InstallContext } from "./InstallFlow";

export default function NewInstall() {
  const overlayContext = useContext(OverlayContext);
  const installContext = useContext(InstallContext);

  if (!installContext.site) return <></>
  
  return (
    <>
      <div>New Install</div>
      <div>Consumer: {`${installContext.site.consumer.firstName} ${installContext.site.consumer.lastName}`}</div>
      <button onClick={() => overlayContext.push('Pod')}>Pod</button>
      <button onClick={() => overlayContext.push('Battery')}>Battery</button>
      <button onClick={() => overlayContext.push('Inverter')}>Inverter</button>
    </>
  );
}
