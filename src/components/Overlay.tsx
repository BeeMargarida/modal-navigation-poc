import { createContext, useEffect, useRef, useState } from "react";
import { Box, Button, Layer, Main } from "grommet";
import { ChevronLeft, X } from "react-feather";

type Props<T> = {
    children: React.ReactElement
    firstRoute?: T
    onClose: () => void
}

export interface IOverlayContext<T> {
    currentRoute?: T;
    push: (entry: T) => void
    pop: (length?: number) => void
}

export const OverlayContext = createContext<IOverlayContext<any>>({
    push: (entry) => { },
    pop: (length?: number) => { }
})

export default function Overlay<T>({ children, firstRoute, onClose }: Props<T>) {
    const [currentRoute, setCurrentRoute] = useState<T | undefined>(firstRoute)
    const history = useRef<T[]>(firstRoute ? [firstRoute] : [])

    useEffect(() => {
        if (!firstRoute) return
        setCurrentRoute(firstRoute)
    }, [])

    const push = (route: T) => {
        history.current = [...history.current, route]
        setCurrentRoute(route)
    }

    const pop = (length = 1) => {
        if (history.current.length === 1) {
            onClose()
            return
        }

        const currentRoute = history.current.length > length ? history.current[history.current.length - length - 1] : undefined
        history.current = history.current.slice(0, history.current.length - length)
        setCurrentRoute(currentRoute)
    }

    return (
        <Layer>
            <Main pad={{ horizontal: 'large', bottom: 'large' }} gap='1rem' >
                <Box pad={{ top: '1rem' }}>
                    {history.current.length <= 1 ? (
                        <Button
                            style={{ padding: 0 }}
                            icon={<X size='21' color='black' />}
                            onClick={onClose}
                        />
                    ) : (
                        <Button

                            icon={<ChevronLeft />}
                            onClick={() => pop()}
                        />
                    )}
                </Box>
                <OverlayContext.Provider value={{
                    currentRoute: currentRoute,
                    push: push,
                    pop: pop
                }}>
                    {children}
                </OverlayContext.Provider>
            </Main>
        </Layer>
    );
}
